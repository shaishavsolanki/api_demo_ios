//
//  ChangeCityInterfaceController.swift
//  APIDemo
//
//  Created by MacStudent on 2019-03-05.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON


class ChangeCityInterfaceController: WKInterfaceController {

    let API_KEY = "3b4723eb9545f0"
    
    // MARK: Outlet
    @IBOutlet var cityLabel: WKInterfaceLabel!
    
    @IBOutlet var chnagecityLoading: WKInterfaceImage!
    // MARK: Variable names
    var cityName:String?
    
    // MARK: Actions
    @IBAction func saveCityButtonPressed() {
        // save the city to shared preferences
        let sharedPreferences = UserDefaults.standard
        sharedPreferences.set(self.cityName, forKey:"city")
        print("Saved \(self.cityName!) to shared preferences!")
        
        self.chnagecityLoading.setImageNamed("Progress")
        self.chnagecityLoading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 0)
        

        
        // convert city to lat/lng
        self.convertToLatLng(city:self.cityName!)
        print("done!")
        
        
//        // send the person back to previous screen
//        self.popToRootController()
        
        
    }
    

    
    @IBAction func pickCityButtonPressed() {
        // add the code to let user do input!!
        
        // 1. When person clicks on button, show them the input UI
        let suggestedResponses = ["Toronto", "Montreal", "Paris", "Rome"]
        
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
           
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.cityLabel.setText(userResponse)
                
                // 3. also save the user's choice to the cityName variable
                self.cityName = userResponse
            }
        }

        
    }
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("I loaded page 2")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    
    func convertToLatLng(city:String) {
        let URL = "https://us1.locationiq.com/v1/search.php?key=\(self.API_KEY)&q=\(city)&format=json&limit=1"
        Alamofire.request(URL).responseJSON {
            // 2. When response comes back:
            
            // store the data from the internet in the
            // response variable
            response in
            
            // get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }

            // convert data to a json dictionary
            let jsonResponse = JSON(apiData)
            print(jsonResponse.rawValue)
            
            // parse out the lat / long parameters
            
            
            let firstItem = jsonResponse.array?.first
            print(firstItem?.description)
            

            let cityInfo = firstItem?.dictionary
            
            let lat = cityInfo?["lat"]?.string
            let long = cityInfo?["lon"]?.string
            
            print("Latitude: \(lat)")
            print("Longitude: \(long)")
            
            let sharedPreferences = UserDefaults.standard
            sharedPreferences.set(lat, forKey:"latitude")
            sharedPreferences.set(long, forKey:"longitude")
            print("Done lat/long saving to shared preferences!")
            
            // Send user back to previous screen
            self.popToRootController()

            
            
//            let lat = firstItem?["lat"].string
//            let long = firstItem?["lon"].string
//
//            print("latitiude: \(lat)")
//            print("longitude: \(long)")
            
            
            // Save the lat/lng in shared preferences
        }

    }
}
