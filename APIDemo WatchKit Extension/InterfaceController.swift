//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON

class InterfaceController: WKInterfaceController {

    // MARK: Outlets
    @IBOutlet var sunriseTimeLabel: WKInterfaceLabel!
    @IBOutlet var sunsetTimeLabel: WKInterfaceLabel!
    @IBOutlet var sunriseLoading: WKInterfaceImage!
    @IBOutlet var sunsetLoading: WKInterfaceImage!
    @IBOutlet var changeCityLoading: WKInterfaceImage!
    
    @IBOutlet var loadingImage: WKInterfaceImage!
    
    
    @IBOutlet var cityNameLabel: WKInterfaceLabel!
    
    
    // MARK: Default functions
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // 1. when the app starts, show the loading animation
        // 2. go get data from website
        // 3. when you get the data,
        //       -stop the animation (hide the animation)
        //       - update the labels with sunrise/sunset data
        
        
        // 0. Check if there is a previously saved city
        let sharedPreferences = UserDefaults.standard
        var city = sharedPreferences.string(forKey: "city")

        if (city == nil) {
            // by default, the strating city is Vancouver
            city = "Vancouver"
            print("No city was set, setting default city to Vancouver")
        }
        else {
            print("I found a city: \(city)")
        }
        
        // update the label to show the current city
        self.cityNameLabel.setText(city)
        
        // 1. start the animation
        self.sunriseLoading.setImageNamed("Progress")
        self.sunriseLoading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 0)
        
        self.sunsetLoading.setImageNamed("Progress")
        self.sunsetLoading.startAnimatingWithImages(in: NSMakeRange(0, 10), duration: 2, repeatCount: 0)
        
        // 2. UPDATE THE LABELS SO USER KNOWS TEH DATA IS LOADING
        self.sunriseTimeLabel.setText("Updating...")
        self.sunsetTimeLabel.setText("Updating...")
        
        // ---------------
        
        
        let sharedPrefrences = UserDefaults.standard
        var lat = sharedPrefrences.value(forKey: "latitude")
        var long =  sharedPrefrences.value(forKey: "longitude")
        
        if (lat == nil || long == nil) {
            // if lat/lng is missing then use default coordinates
            // vancouver
            lat = "45.7411"
            long = "-71.8523"
        }

        let URL = "https://api.sunrise-sunset.org/json?lat=\(lat!)&lng=\(long!)&date=today"
       
//        URL.replacingOccurrences(of:"49.2827", with:lat)
//        URL.replacingOccurrences(of:"-123.1207", with:lng)
//
//
   
        Alamofire.request(URL).responseJSON {
            // 1. store the data from the internet in the
            // response variable
            response in
            
            // 2. get the data out of the variable
            guard let apiData = response.result.value else {
                print("Error getting data from the URL")
                return
            }
            
            // OUTPUT the json response to the terminal
            print(apiData)
            
            
            // GET something out of the JSON response
            let jsonResponse = JSON(apiData)
            let sunriseTime = jsonResponse["results"]["sunrise"].string
            let sunsetTime = jsonResponse["results"]["sunset"].string
            
            print("Sunrise: \(sunriseTime)")
            print("Sunset: \(sunsetTime)")
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "hh:mm:ss a"
//
//            let convertedDate = formatter.date(from: sunsetTime!)
//            formatter.timeZone
//
            
            // stop the animation
            self.sunriseLoading.stopAnimating()
            self.sunsetLoading.stopAnimating()
            // and then hide it
            
            self.sunriseLoading.setImageNamed(nil)
            self.sunsetLoading.setImageNamed(nil)
            
            
            // show the sunrise and sunset in the IPhone App
            self.sunriseTimeLabel.setText("\(sunriseTime!)")
            self.sunsetTimeLabel.setText("\(sunsetTime!)")
            
        }
        
        // ---------------
    }
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
